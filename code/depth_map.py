"""
Author: Marek Marusic & Jan Herec
Project: FIT VUT, ZPO: Depth map
Description: main file which parse arguments and run selected methods for creating disparity and 3D model from stereo images
"""

import cv2 as cv
from utils import Utils
import sys
import os

import cv2

import graph_cuts
import json
import numpy as np
import argparse

import local_method

# try use MPI

USE_MPI = False
rank = 0
num_procs = 1

try:
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    num_procs = comm.Get_size()
    rank = comm.Get_rank()
    if num_procs > 1:
        USE_MPI = True
except Exception as e:
    pass

allMethods = ["gc_ssd", "gc_by_pix", "loc_by_pix", "loc_by_block", "loc_by_pix_adaptive"]


def parseConfig(config, methods):
    c = {"gc_ssd": None,
         "gc_by_pix": None,
         "loc_by_pix": None,
         "loc_by_block": None,
         "loc_by_pix_adaptive": None}
    for m in methods:
        if config and m in config:
            c[m] = fillMissingValuesToConfig(config[m])
        else:
            c[m] = fillMissingValuesToConfig(None)
    return c


def fillMissingValuesToConfig(methodConf):
    conf = {}
    conf["disp_min"] = methodConf["disp_min"] if methodConf and "disp_min" in methodConf else -50
    conf["disp_max"] = methodConf["disp_max"] if methodConf and "disp_max" in methodConf else 50
    conf["smoothness"] = methodConf["smoothness"] if methodConf and "smoothness" in methodConf else 1.0
    conf["windowSize"] = methodConf["windowSize"] if methodConf and "windowSize" in methodConf else 1
    conf["cycles"] = methodConf["cycles"] if methodConf and "cycles" in methodConf else 3

    conf["min_block_size"] = methodConf["min_block_size"] if methodConf and "min_block_size" in methodConf else 5
    conf["num_disparities"] = methodConf["num_disparities"] if methodConf and "num_disparities" in methodConf else None
    conf["searched_window_w"] = methodConf["searched_window_w"] if methodConf and "searched_window_w" in methodConf else 80
    conf["searched_window_h"] = methodConf["searched_window_h"] if methodConf and "searched_window_h" in methodConf else 15
    conf["blocksize"] = methodConf["blocksize"] if methodConf and "blocksize" in methodConf else 9
    conf["use_medianblur"] = methodConf["use_medianblur"] if methodConf and "use_medianblur" in methodConf else True
    conf["median_window_size"] = methodConf["median_window_size"] if methodConf and "median_window_size" in methodConf else 11

    return conf


def parseArguments():
    global allMethods

    # Default settings
    arguments = {"imgL": "imL.png", "imgR": "imR.png", "calibFile": "calib.txt", "methods": allMethods,
                 "outputPrefix": "", "scale": 1,
                 "config": {"gc_ssd": None,
                            "gc_by_pix": None,
                            "loc_by_pix": None,
                            "loc_by_block": None,
                            "loc_by_pix_adaptive": None}}

    parser = argparse.ArgumentParser()
    parser.add_argument("--methods", nargs='+',
                        help="Which method to use",
                        choices=allMethods + ["all"])
    parser.add_argument("--config",
                        help="Method params as json file. Default is config.json")
    parser.add_argument("--bothImg",
                        help="Path to image containing both left image and right image.")
    parser.add_argument("--leftImg",
                        help="Path to left image.")
    parser.add_argument("--rightImg",
                        help="Path to right image.")
    parser.add_argument("--calib",
                        help="File with camera calibration info.")
    parser.add_argument("--dataDir",
                        help="Directory containing calib.txt, imL.png and imR.png.")
    parser.add_argument("--outputPrefix",
                        help="Filename prefix used for the output data.")
    parser.add_argument("--scale", type=float,
                        help="Scale of input image.")

    args = parser.parse_args()

    # get input images
    if args.bothImg:
        imgL, imgR = Utils.get_left_and_right_images_from_single_image(args.bothImg)
    elif args.leftImg and args.rightImg:
        imgL = cv.imread(args.leftImg)
        imgR = cv.imread(args.rightImg)
    elif args.dataDir:
        imgL = cv.imread("%s/im0.png" % args.dataDir)
        imgR = cv.imread("%s/im1.png" % args.dataDir)
    else:
        print("Error: parameter bothImg or parameter dataDir or both parameters leftImg and rightImg must be set.")
        sys.exit(-1)

    if imgL is None or imgR is None:
        print("Error: parameter bothImg or parameter dataDir or both parameters leftImg and rightImg must be set.")
        sys.exit(-1)

    # get calib file
    if args.bothImg or (args.leftImg and args.rightImg):
        if args.calib:
            calibFile = args.calib
        else:
            print("Error: calib file must be set.")
            sys.exit(-1)
    elif args.dataDir:
        calibFile = "%s/calib.txt" % args.dataDir

    # try read calib file
    with open(calibFile) as f:
        pass

    # get method
    if args.methods and "all" not in args.methods:
        methods = args.methods
    else:
        methods = allMethods
    # get method params
    if args.config and os.path.isfile(args.config):
        # try read method params json file
        config = json.load(open(args.config))
    elif os.path.isfile("config.json"):
        # try load default config
        config = json.load(open("config.json"))
    else:
        print("Warning: No config")
        config = None
    config = parseConfig(config, methods)

    # get outouts prefix
    if args.outputPrefix:
        outputPrefix = args.outputPrefix
        os.makedirs(os.path.dirname(outputPrefix), exist_ok=True)
    else:
        outputPrefix = "./out-"

    # get scale
    if args.scale:
        scale = float(args.scale)
    else:
        scale = 1.0

    arguments["imgLOriginal"] = imgL
    arguments["imgROriginal"] = imgR
    arguments["imgL"] = scaleImage(imgL, scale)
    arguments["imgR"] = scaleImage(imgR, scale)
    arguments["calibFile"] = calibFile
    arguments["methods"] = methods
    arguments["config"] = config
    arguments["outputPrefix"] = outputPrefix
    arguments["scale"] = scale

    return arguments


def scaleImage(img, scale):
    # downscale images for faster processing
    return cv.resize(img, (int(img.shape[1] * scale), int(img.shape[0] * scale)))


def evalMethodWithParams(method, config, imgL, imgR):
    global allMethods
    # allMethods = ["gc_ssd", "gc_by_pix", "loc_by_pix", "loc_by_block", "loc_by_pix_adaptive"]

    if method == allMethods[0]:
        return graph_cuts.disparityGraphCutsWithSSD(imgL,
                                                    imgR,
                                                    config["disp_min"],
                                                    config["disp_max"],
                                                    config["smoothness"],
                                                    config["windowSize"],
                                                    config["cycles"]
                                                    )
    elif method == allMethods[1]:
        return graph_cuts.disparityGraphCutsByPix(imgL,
                                                  imgR,
                                                  config["disp_min"],
                                                  config["disp_max"],
                                                  config["smoothness"],
                                                  config["cycles"]
                                                  )
    elif method == allMethods[2] or method == allMethods[3] or method == allMethods[4]:
        return local_method.local_method(method, imgL, imgR, config)


if __name__ == '__main__':
    from ply import saveToPLY

    arguments = parseArguments()
    """for arg in arguments:
        print(arg)
        print(arguments[arg])"""

    # for selected methods we apply these method to create disparities and 3D model
    for method in arguments["methods"]:

        if USE_MPI:
            comm.Barrier()
        if (method in ["gc_ssd", "gc_by_pix"] and rank == 0) or (method in ["loc_by_pix", "loc_by_block", "loc_by_pix_adaptive"]):
            print(method)
            disparityMap = evalMethodWithParams(method, arguments["config"][method], arguments["imgL"], arguments["imgR"])
            if disparityMap is None:
                print("Error: There is no disparity computed.")
                continue

        # only one process (eg. master) can write results
        if rank == 0:
            outFileName = arguments["outputPrefix"] + method
            saveToPLY(outFileName + ".ply",
                      disparityMap,
                      arguments["imgL"],
                      calibFile=arguments["calibFile"],
                      scale=arguments["scale"])
            # Normalize disparity to 0 - 255 interval
            disparityMap = (disparityMap * 255.0 / np.amax(disparityMap)).astype(dtype=np.uint8)
            cv2.imwrite(outFileName + ".png", disparityMap)

    if USE_MPI:
        comm.Barrier()
        #comm.Abort()
    sys.exit()
