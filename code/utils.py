"""
Author: Marek Marusic & Jan Herec
Project: FIT VUT, ZPO: Depth map
Description: utils
"""

from __future__ import print_function

import cv2 as cv
import pathlib

from camera import Camera


class Utils(object):
    @staticmethod
    def get_left_and_right_images_from_single_image(image_path):
        """
        Methods return extracted left and right imgaes from single image which contains these two images
        :author Jan Herec
        :param image_path: path to image
        :return: tuple (imgL, imgR)
        """

        img = cv.imread(image_path)
        imgL_x_start = 0
        imgL_x_end = img.shape[1] // 2
        imgL_y_start = 0
        imgL_y_end = img.shape[0]

        imgR_x_start = img.shape[1] // 2
        imgR_x_end = img.shape[1]
        imgR_y_start = 0
        imgR_y_end = img.shape[0]

        imgL = img[imgL_y_start: imgL_y_end, imgL_x_start: imgL_x_end]
        imgR = img[imgR_y_start: imgR_y_end, imgR_x_start: imgR_x_end]

        return imgL, imgR

    @staticmethod
    def calcDepth(disparity, calibFile, scale):
        cam = Camera()
        cam.calibrate(disparity, calibFile, scale)
        return cam.baseline * cam.f / (disparity + cam.doffs)

    @staticmethod
    def splitImages():
        from os import listdir
        from os.path import isfile, join
        path = "data/zed/"
        pathToSplitted = path+'splitted'

        pathlib.Path(pathToSplitted).mkdir(exist_ok=True)

        for f in listdir(path):
            if isfile(join(path, f)) and f.endswith('png') and "Explorer" in f:
                imgL, imgR = Utils.get_left_and_right_images_from_single_image(join(path, f))
                ftype = ".png"
                fname = f.strip(ftype)

                cv.imwrite(join(pathToSplitted, fname+"L"+ftype), imgL)
                cv.imwrite(join(pathToSplitted, fname+"R"+ftype), imgR)


if __name__ == '__main__':
    Utils.splitImages()





