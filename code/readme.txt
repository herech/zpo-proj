Autori: Jan Herec a Marek Marusic
Popis: Program pro vytvoreni mapy disparity a 3D modelu ze stereo obrazku. K vytvoreni mapy disparity je mozne pouzit vybrane metody z celkoveho mnozstvi 5 metod (2 globalni a 3 lokalni)
Rozbehnuti a pouziti programu: 
1. - Je třeba nainstalovat Python 3 a OpenCV 3 (OpenCV binarky s pripravenymi bindy pro Python lze nainstalovat pomocí: python -m pip install opencv-contrib-python)
   - Dále je třeba mít knihovny numpy a pymaxflow (python -m pip install numpy pymaxflow) a mozna i dalsi knihovny.

2. - Pokud mate zajem o paralelizaci lokalnich metod, pote je treba mit nainstalovanou implementaci MPI (OpenMPI, MPICH, Microsoft MPI, Intel MPI) a prislusny balicek mpi4py (python -m pip install mpi4py)
   - Pokud mate zajem o prohlizeci vyslednych 3D modelu prostredi .ply, doporucuje se nainstalovat MeshLab

3. - Podrobnejsi info k pouziti programu a jeho vstupnim parametrum je v dokumentaci k programu ve formatu pdf, ktera neni soucasti tohoto archivu.

4. - Slozka showcase ukazuje jak pro stereo obrazky vypadaji mapy disparit. Stereo obrazky im1.png a im0.png slouzi take jako ukazkove vstupni soubory, na ktere je mozne program aplikovat.
   - Dalsi stereo obrazky na kterych je mozne program zkouset jsou nase vlastnorucne nasnimane obrazky pomoci ZED kamery: https://uloz.to/!fSui8kVvz3lQ/zed-camera-stereo-dataset-fit-vut-zip
   - A taky se doporucujeme podivat na http://vision.middlebury.edu/stereo/data/scenes2014/datasets/

5. - Program se spousti pomoci [mpiexec] python3 depth_map.py parametry
   - Pro ukazku funkce programu se doporucuje spustit: 
       a) Pokud chceme vyzkouset vsechny implementovane metody (pomale): [mpiexec] python3 depth_map.py --dataDir=showcase/teddy
	   b) Pokud chcete vyzkouset jednu lokalni a jednu globalmi metodu (rychlejsi, pro ukazku funkcnosti dostacujici): [mpiexec] python3 depth_map.py --dataDir=showcase/teddy --methods gc_by_pix loc_by_pix