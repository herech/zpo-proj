"""
Author: Marek Marusic
Project: FIT VUT, ZPO: Depth map
Description: global (graph cuts) methods to create disparity maps from stereo images
"""

import cv2 as cv

import numpy as np
from scipy import ndimage

import fastmin


def findDisparitiesByPixel(img1, img2, disp_min, disp_max):
    # img1 - left image
    # img2 - right image
    # theory https://www.dropbox.com/s/6t99iccihagdcmg/YSC3221_Lecture14.pdf?dl=0

    D = np.full((img1.shape[0], img1.shape[1], int(disp_max - disp_min)), np.inf)

    cols = img1.shape[1]

    for row in range(0, img1.shape[0]):
        for col in range(0, img1.shape[1]):
            for idx, d in enumerate(np.arange(disp_min, disp_max)):
                if 0 <= col+d < cols:
                    diff = abs(img1[row, col] - img2[row, col + d]) ** 2
                else:
                    diff = np.inf
                D[row, col, idx] = diff
    return D


def localssd(im1, im2, K):
    # Source https://github.com/pmneila/PyMaxflow/blob/master/examples/stereogram_solver.py
    """
    The local sum of squared differences between windows of two images.

    The size of each window is (2K+1)x(2K+1).
    """
    H = np.ones((2 * K + 1, 2 * K + 1))
    diff2 = (im1 - im2) ** 2
    if im1.ndim == 2:
        return ndimage.convolve(diff2, H, mode='constant')
    if diff2.ndim == 3 and 3 <= diff2.shape[-1] <= 4:
        res = np.empty_like(diff2)
        for channel in range(diff2.shape[-1]):
            res[..., channel] = ndimage.convolve(diff2[..., channel], H, mode='constant')
        return res.sum(2)
    raise ValueError("invalid number of dimensions for input images")


def ssd_volume(im1, im2, disps, K):
    # Source https://github.com/pmneila/PyMaxflow/blob/master/examples/stereogram_solver.py
    """
    Compute the visual similarity between local windows of the image 1
    and the image 2 displaced horizontally.

    The two images are supposed to be rectified so that the epipolar
    lines correspond to the scan-lines. The image 2 is horizontally
    moved between w[0] and w[1] pixels and, for each value, the local
    similarity between the first image and the displaced second image
    is computed calling to localssd.

    The result matrix is defined so that the value D[l, i, j] is the
    SSD of the local windows between the window centered at im1[i,j]
    and the window centered at im2[i, j + l + w[0]]. In the event that
    the value im2[i, j + l + w[0]] was not defined, D[l, i, j] will be
    very large.
    """
    D = np.full((im1.shape[0], im1.shape[1], len(disps)), np.inf)

    for idx, d in enumerate(disps):
        g = np.zeros(im2.shape)
        if d < 0:
            g[:, -d:] = im2[:, :d]
        elif d > 0:
            g[:, :-d] = im2[:, d:]
        else:
            g = im2
        aux = localssd(im1, g, K)
        D[..., idx] = aux

    return D


def disparityGraphCutsWithSSD(img1, img2, disp_min, disp_max, smoothness, windowSize, cycles=3):
    """img must be an array of np.float_"""
    #print(img1.shape[0])
    #print(img1.shape[1])

    # Convert to BW
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    img2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

    # Normalize in order to fit the results to uint8
    norm = 16.0
    img1 = img1/norm
    img2 = img2/norm
    # Calculate disparities
    D = ssd_volume(img1, img2, np.arange(disp_min, disp_max), windowSize)
    num_labels = D.shape[-1]
    X, Y = np.mgrid[:num_labels, :num_labels]
    V = smoothness * np.float_(np.abs(X - Y))
    sol = fastmin.aexpansion_grid(D, V, cycles)
    # sol = fastmin.energy_of_grid_labeling(D, V, 5)

    # Convert to int
    sol = sol.astype(dtype=int)
    # Invert
    sol = np.amax(sol) - sol
    # Normalize
    # result = (sol * 255.0 / np.amax(sol)).astype(dtype=np.uint8)
    # result = sol.astype(dtype=np.uint8)
    result = sol
    return result


def disparityGraphCutsByPix(img1, img2, disp_min, disp_max, smoothness, cycles=3):
    """img must be an array of np.float_"""
    #print(img1.shape[0])
    #print(img1.shape[1])
    img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)
    img2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)
    norm = 16.0
    img1 = img1/norm
    img2 = img2/norm
    D = findDisparitiesByPixel(img1, img2, disp_min, disp_max)
    num_labels = D.shape[-1]
    X, Y = np.mgrid[:num_labels, :num_labels]
    V = smoothness * np.float_(np.abs(X - Y))
    sol = fastmin.aexpansion_grid(D, V, cycles)

    # Convert to int
    sol = sol.astype(dtype=int)
    # Invert
    sol = np.amax(sol) - sol
    # Normalize
    # result = (sol * 255.0 / np.amax(sol)).astype(dtype=np.uint8)
    # result = sol.astype(dtype=np.uint8)
    result = sol
    return result


if __name__ == '__main__':
    imgL = cv.imread('workdir/imL-BW.png')
    imgR = cv.imread('workdir/imR-BW.png')

    res = disparityGraphCutsByPix(imgL, imgR, -50, 50, 1.0)
    # res = disparityGraphCutsWithSSD(imgL, imgR, -50, 50, 2.0, 1, cycles=3)

    cv.imwrite("disparity.png", res)
    cv.imshow('origL', imgL)
    cv.imshow('disparity1', res)
    cv.waitKey()
    cv.destroyAllWindows()
