#!/usr/bin/env python

"""
Author: Jan Herec
Project: FIT VUT, ZPO: Depth map
Description: local methods to create disparity maps from stereo images
"""

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2 as cv
import os

from camera import Camera
from utils import Utils

import numpy as np

# try use MPI to paralelize image processing

USE_MPI = False
rank = 0
num_procs = 1

try:
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    num_procs = comm.Get_size()
    rank = comm.Get_rank()
    if num_procs > 1:
        USE_MPI = True
except Exception as e:
    pass


def createReferenceDisparity(imgL, imgR):
    """
    OpenCV method for creating referece disparity
    :param imgL: left image
    :param imgR: right image
    :return: computed disparity
    """

    window_size = 3
    min_disp = 16
    num_disp = 112 - min_disp
    stereo = cv.StereoSGBM_create(minDisparity=min_disp,
                                  numDisparities=num_disp,
                                  blockSize=16,
                                  P1=8 * 3 * window_size ** 2,
                                  P2=32 * 3 * window_size ** 2,
                                  disp12MaxDiff=1,
                                  uniquenessRatio=10,
                                  speckleWindowSize=100,
                                  speckleRange=32
                                  )

    disp = stereo.compute(imgL, imgR).astype(np.float32) / 16.0

    # disp = (disp-min_disp)/num_disp

    return disp

def local_method(method, imgL, imgR, settings = None):
    """
    Methods create disparity map from two stereo images
    :author Jan Herec
    :param imgL: left stereo image
    :param imgR: right stereo image
    :param settings algorithm setting
    :return: disparity map
    """

    # allMethods = ["gc_ssd", "gc_by_pix", "loc_by_pix", "loc_by_block", "loc_by_pix_adaptive"]

    imgLDisparity = cv.cvtColor(imgL, cv.COLOR_BGR2GRAY)
    imgL = cv.cvtColor(imgL, cv.COLOR_BGR2GRAY)
    imgR = cv.cvtColor(imgR, cv.COLOR_BGR2GRAY)

    searched_window_w = settings["searched_window_w"]
    searched_window_h = settings["searched_window_h"]
    blocksize = settings["blocksize"]

    block_vertical_offset = (searched_window_h - blocksize) // 2 # vertical offset searched block in searched wondows

    imgL_width = imgL.shape[1]
    imgL_height = imgL.shape[0]

    first_vertical_line = 0
    last_vertical_line = 0

    # if method is per pixel
    if method == "loc_by_pix" or method == "loc_by_pix_adaptive":
        block_vertical_count = imgL_height - blocksize // 2
        block_horizontal_count = imgL_width - blocksize // 2

        first_block = rank * (block_vertical_count // num_procs)        # first block vertical offset
        last_block = first_block + (block_vertical_count // num_procs)  # last block vertical offset

        # set ranges fo adaptive window
        up_range = blocksize
        low_range = blocksize - 1
        range_step = -2

        # set lower posibble block size
        if method == "loc_by_pix_adaptive":
            low_range = settings["min_block_size"] - 1

        num_adaptive_blocks = len(list(range(up_range, low_range, range_step)))

        # process image horizontal stripe which belong to this process/rank
        for i in range(first_block, last_block):
            for j in range(block_horizontal_count):

                # save disparities and their cost for particular block size
                disparity_list = []
                min_cost_list = []

                for k in range(num_adaptive_blocks): # we try different adaptive block sizes with step size -2 from max size to min size
                    current_block = imgL[i+k:i+blocksize-k, j+k:j+blocksize-k]
                    top_corner_searched_windows = i - block_vertical_offset
                    left_corner_searched_windows = j - searched_window_w

                    # when set searched window in bounds of right image
                    if (top_corner_searched_windows < 0):
                        top_corner_searched_windows = 0
                    elif (top_corner_searched_windows > imgL_height - searched_window_h):
                        top_corner_searched_windows = imgL_height - searched_window_h

                    if (left_corner_searched_windows < 0):
                        left_corner_searched_windows = 0

                    current_searched_window = imgR[top_corner_searched_windows:top_corner_searched_windows+searched_window_h, left_corner_searched_windows:left_corner_searched_windows+searched_window_w]

                    matched_block = cv.matchTemplate(current_searched_window, current_block, cv.TM_SQDIFF)
                    min_value, max_value, min_val_block_location, max_val_block_location = cv.minMaxLoc(matched_block)
                    disparity = abs((left_corner_searched_windows + min_val_block_location[0]) - j)

                    disparity_list.append(disparity) # append disparity for current block size
                    min_cost_list.append(min_value)  # append cost for current disparity for current block size

                min_cost = min(min_cost_list)
                min_index = min_cost_list.index(min_cost)
                imgLDisparity[i + blocksize // 2, j + blocksize // 2] = disparity_list[min_index] # get disparity with low cost

            # when use mpi we get vertical stripe coordinates
            if USE_MPI:
                if i == first_block:
                    first_vertical_line = i + blocksize // 2
                if i == last_block - 1:
                    last_vertical_line = (i + blocksize // 2) + 1

    # if method is per block
    elif method == "loc_by_block":

        block_vertical_count = imgL_height // blocksize
        block_horizontal_count = imgL_width // blocksize

        first_block = rank * (block_vertical_count // num_procs)        # first block vertical offset
        last_block = first_block + (block_vertical_count // num_procs)  # last block vertical offset

        # process image horizontal stripe which belong to this process/rank
        for i in range(first_block, last_block):
            for j in range(block_horizontal_count):
                current_block = imgL[(i*blocksize):((i*blocksize)+blocksize), (j*blocksize):((j*blocksize)+blocksize)]
                top_corner_searched_windows = (i*blocksize) - block_vertical_offset
                left_corner_searched_windows = (j*blocksize) - searched_window_w

                # when set searched window in bounds of right image
                if (top_corner_searched_windows < 0):
                    top_corner_searched_windows = 0
                elif (top_corner_searched_windows > imgL_height - searched_window_h):
                    top_corner_searched_windows = imgL_height - searched_window_h

                if (left_corner_searched_windows < 0):
                    left_corner_searched_windows = 0

                current_searched_window = imgR[top_corner_searched_windows:top_corner_searched_windows+searched_window_h, left_corner_searched_windows:left_corner_searched_windows+searched_window_w]

                matched_block = cv.matchTemplate(current_searched_window, current_block, cv.TM_SQDIFF)
                min_value, max_value, min_val_block_location, max_val_block_location = cv.minMaxLoc(matched_block)
                disparity = abs((left_corner_searched_windows + min_val_block_location[0]) - (j*blocksize))

                imgLDisparity[(i*blocksize):((i*blocksize)+blocksize), (j*blocksize):((j*blocksize)+blocksize)] = disparity # get disparity with low cost

            # when use mpi we get vertical stripe coordinates
            if USE_MPI:
                if i == first_block:
                    first_vertical_line = i*blocksize
                if i == last_block - 1:
                    last_vertical_line = (i*blocksize)+blocksize

    # when use mpi we collect processed stripes (partial disparity maps) from processes to master process with rank 0
    if USE_MPI:

        sendbuf = imgLDisparity[first_vertical_line : last_vertical_line, :]
        recvbuf = None
        if rank == 0:
            recvbuf = np.empty([num_procs, (last_vertical_line - first_vertical_line), imgL_width], dtype=np.uint8)
        comm.Gather(sendbuf, recvbuf, root=0)

        if rank == 0:
            imgLDisparity_temp = np.concatenate(list(recvbuf), axis=0)

            imgLDisparity[first_vertical_line: first_vertical_line + imgLDisparity_temp.shape[0], 0: imgLDisparity_temp.shape[1]] = imgLDisparity_temp

    if settings["use_medianblur"]:
        imgLDisparity = cv.medianBlur(imgLDisparity, settings["median_window_size"])

    # discretize disparities = better segmentation of object
    if (settings["num_disparities"] != None):
        max = np.amax(imgLDisparity)
        step = (max - blocksize) // settings["num_disparities"]
        step = 1 if step == 0 else step
        imgLDisparity = np.ceil((imgLDisparity / step)) * step
        imgLDisparity = imgLDisparity.astype(np.uint8)
    return imgLDisparity
