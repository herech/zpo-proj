
class Camera:
    def __init__(self):
        self.f = 0
        self.cx = 0
        self.cy = 0
        self.doffs = 0
        self.baseline = 0

    def calibrate(self, imgL = None, calibFile=None, scale=1):
        if calibFile is not None:
            # f = 3962.004  # guess for focal length
            # cx = 1146.717
            # cy = 975.476
            # doffs = 107.911
            # baseline = 237.604
            with open(calibFile, 'rb') as calib_file:
                for line in calib_file.readlines():
                    line = line.decode('UTF-8').rstrip('\n')
                    if len(line) == 0:
                        continue
                    eq_pos = line.find('=')
                    if eq_pos < 0:
                        raise Exception('Cannot parse calib file: ')
                    param = line[:eq_pos]
                    val = line[eq_pos + 1:]

                    if param == "cam0":
                        cam0Vals = val.strip("[]").replace(";", "").split()
                        self.f = float(cam0Vals[0]) * scale
                        self.cx = float(cam0Vals[2]) * scale
                        self.cy = float(cam0Vals[5]) * scale
                    elif param == "doffs":
                        self.doffs = float(line[eq_pos + 1:]) * scale
                    elif param == "baseline":
                        self.baseline = float(line[eq_pos + 1:]) * scale
        elif imgL is not None:
            h, w = imgL.shape[:2]
            self.cx = 0.5 * w
            self.cy = 0.5 * h
            self.f = 0.8 * w
            self.doffs = 0
            self.baseline = 1
        else:
            pass


if __name__ == '__main__':
    fileName = "datasets/Classroom1-perfect/calib.txt"
    cam = Camera()
    cam.calibrate(calibFile=fileName)
    print(cam)
