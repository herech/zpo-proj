import numpy as np
import cv2

from camera import Camera

# Usage saveToPLY("our.ply", disp, imgL, calibFile="/home/mmarusic/MyDevel/ZPO/zpo/datasets/Classroom1-perfect/calib.txt")


# source https://github.com/opencv/opencv/blob/master/samples/python/stereo_match.py
ply_header = '''ply
format ascii 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''


def write_ply(fn, verts, colors):
    # source https://github.com/opencv/opencv/blob/master/samples/python/stereo_match.py
    verts = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts = np.hstack([verts, colors])
    with open(fn, 'wb') as f:
        f.write((ply_header % dict(vert_num=len(verts))).encode('utf-8'))
        np.savetxt(f, verts, fmt='%f %f %f %d %d %d ')


def saveToPLY(fileName, disparityArray, imgL, calibFile=None, scale = 1):
    print('generating 3d point cloud...', )
    cam = Camera()
    cam.calibrate(imgL, calibFile, scale)

    # https://stackoverflow.com/questions/27374970/q-matrix-for-the-reprojectimageto3d-function-in-opencv
    Q = np.float32([[1, 0, 0, -cam.cx],
                    [0, -1, 0, cam.cy],  # turn points 180 deg around x-axis,
                    [0, 0, 0, -cam.f],  # so that y-axis looks up
                    [0, 0, 1 / cam.baseline, cam.doffs / cam.baseline]])
    # source https://github.com/opencv/opencv/blob/master/samples/python/stereo_match.py
    points = cv2.reprojectImageTo3D(disparityArray, Q)
    colors = cv2.cvtColor(imgL, cv2.COLOR_BGR2RGB)
    mask = disparityArray > disparityArray.min()
    out_points = points[mask]
    out_colors = colors[mask]

    write_ply(fileName, out_points, out_colors)
    print('%s saved' % fileName)

